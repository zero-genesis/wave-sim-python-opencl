/**
 * @description
 * Orchestrating Electric Dynamics
 * This function governs the evolution of the electric field in our domain,
 * adapting to the flows and shifts of the underlying magnetic fields.
 */
__kernel void 
evolve_electric_field(
    __global float* electric_field,
    __global float* magnetic_field_x,
    __global float* magnetic_field_y, 
    __global float* permittivity,
    float time_step,
    float space_step_x,
    float space_step_y,
    int grid_width,
    int grid_height
)
{
    
    int pos_x = get_global_id(0);  // Position along x-axis
    int pos_y = get_global_id(1);  // Position along y-axis

    // Ensure our operations remain within the bounds of the simulation grid.
    if(pos_x < grid_width && pos_y < grid_height) 
    {
        // Calculate the influence of magnetic fields on the electric dynamics.
        float change_in_Hy_along_x = pos_x > 0 ? (magnetic_field_y[pos_x * grid_height + pos_y] - magnetic_field_y[(pos_x - 1) * grid_height + pos_y]) / space_step_x : 0.0f;
        float change_in_Hx_along_y = pos_y > 0 ? (magnetic_field_x[pos_x * grid_height + pos_y] - magnetic_field_x[pos_x * grid_height + pos_y - 1]) / space_step_y : 0.0f;
        
        // Modify the electric field based on the magnetic field's influence.
        electric_field[pos_x * grid_height + pos_y] += (time_step / permittivity[pos_x * grid_height + pos_y]) * (change_in_Hy_along_x - change_in_Hx_along_y);
    }
}

/**
 * @description
 * Directing Magnetic Rhythms
 * Responsible for shaping the magnetic fields in our slice of space,
 * this function ensures they respond aptly to the electric field's cues.
 */
__kernel void 
evolve_magnetic_fields(
    __global float* electric_field,
    __global float* magnetic_field_x,
    __global float* magnetic_field_y,
    __global float* permeability,
    float time_step,
    float space_step_x,
    float space_step_y,
    int grid_width,
    int grid_height
) 
{
    
    int pos_x = get_global_id(0);  // Position along x-axis
    int pos_y = get_global_id(1);  // Position along y-axis

    // Remain within the set dimensions of our simulation space.
    if(pos_x < grid_width && pos_y < grid_height) 
    {
        // Understand the cues given by the electric field's shifts.
        float change_in_Ez_along_y = pos_y < grid_height - 1 ? (electric_field[pos_x * grid_height + pos_y + 1] - electric_field[pos_x * grid_height + pos_y]) / space_step_y : 0.0f;
        float change_in_Ez_along_x = pos_x < grid_width - 1 ? (electric_field[(pos_x + 1) * grid_height + pos_y] - electric_field[pos_x * grid_height + pos_y]) / space_step_x : 0.0f;
        
        // Adjust the magnetic fields based on these cues.
        magnetic_field_x[pos_x * grid_height + pos_y] -= (time_step / permeability[pos_x * grid_height + pos_y]) * change_in_Ez_along_y;
        magnetic_field_y[pos_x * grid_height + pos_y] += (time_step / permeability[pos_x * grid_height + pos_y]) * change_in_Ez_along_x;
    }
}

/**
 * @description
 * Birth of an Electromagnetic Wave
 * This kernel introduces an electromagnetic wave into our environment by injecting energy
 * at a specified point, paving the path for its majestic journey through space.
 */
__kernel void 
introduce_point_source(
    __global float* electric_field, 
    int source_x,
    int source_y,
    float magnitude
) 
{
    
    int pos_x = get_global_id(0);  // Position along x-axis
    int pos_y = get_global_id(1);  // Position along y-axis

    // Check if we're at the source position.
    if(pos_x == source_x && pos_y == source_y) 
    {
        electric_field[pos_x * source_y + pos_y] += magnitude;
    }
}
